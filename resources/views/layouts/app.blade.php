<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Drixo - Responsive Booststrap 4 Admin & Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
               @include('admin.partials.header_links')
    </head>
   <body>
    <div id="preloader">
       <div id="status">&nbsp;</div>
    </div>
         @include('admin.partials.header')
             @include('admin.partials.left_sidebar')
             @yield('contents')
          </div>
        </div>   		        
	      @include('admin.partials.footer_links')
</body>
</html>
