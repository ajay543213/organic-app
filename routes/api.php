<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| Author              :   Ajay Maurya(Software Engineer)
| Start Date          :   29/08/2020
| Purpose             :   Used To Routing
| Last Modified Date  :   NULL
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/getUser', 'API\UserController@getUserData');
Route::group(['middleware' => ['\App\Http\Middleware\Webservice','webService']], function($app){
	Route::post('/setUser', 'API\UserController@saveUserData');
	Route::post('/setLocation', 'API\UserController@setLocation');
	Route::post('/setFarmerRequest', 'API\FarmerProductRequestDetails@setFarmerRequest');
	Route::post('/setFarmerProductSellRequest', 'API\FarmerProductRequestDetails@setFarmerProductSellRequest');
	Route::post('/getProducts', 'API\ProductDetailsController@getProducts');
	Route::post('/getGovernmentScheme', 'API\GovernmentSchemesController@getGovernmentScheme');
	Route::post('/getRecipe', 'API\FarmerProductRequestDetails@getRecipe');
	Route::post('/getOrganicStores', 'API\FarmerProductRequestDetails@getOrganicStores');
	
	Route::post('/setBuyerEnquiry', 'API\BuyerController@setBuyerEnquiry');
	Route::post('/getTeaBoardOffice', 'API\BuyerController@getTeaBoardOffice');
	Route::post('/getFarmerFederations', 'API\FarmerProductRequestDetails@getFarmerFederations');
	Route::post('/getOrganicFarmingVideo', 'API\FarmerProductRequestDetails@getOrganicFarmingVideo');
	Route::post('/getOrganicUttrakhandSupport', 'API\FarmerProductRequestDetails@getOrganicUttrakhandSupport');
	Route::post('/getCAOData', 'API\FarmerProductRequestDetails@getCAOData');
	Route::post('/getFarmerData', 'API\FarmerProductRequestDetails@getFarmerData');
	Route::post('/setFcmId', 'API\FarmerProductRequestDetails@setFcmId');
	Route::post('/facilityAndFee', 'API\FarmerProductRequestDetails@facilityAndFee');

});	

Route::fallback(function(){
    return response()->json([
    	'status'    => false,
    	'message'   => 'Page Not Found.',
	], 404);
});
