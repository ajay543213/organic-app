ALTER TABLE `users` CHANGE `createdAt` `createdAt` BIGINT NULL DEFAULT NULL;
ALTER TABLE `users` CHANGE `updatedAt` `updatedAt` BIGINT NULL DEFAULT NULL;
ALTER TABLE `users` CHANGE `userType` `userType` INT(255) NOT NULL;