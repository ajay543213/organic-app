<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Request as HttpRequest;
use Request;
use Validator;
use Image;
trait Imageable{

  protected $uploadPath;

  public function store(HttpRequest $request)
  {

    $requestData = Request::file();
    $validator = Validator::make($requestData, [
      'file' => ['required', 'image'],
    ]);
    if ($validator->fails()) {
        return [
          'error' => $validator
        ];
    }else{
      $file                           = $request->file('file');
      $file_size                      =    $file->getClientSize();
      $total_file_size                = ($file_size)/1024;
      $total_file_size                = ($total_file_size)/1024;

      if($total_file_size == 0 || $total_file_size > 2){
          $parameters = $this->getDestination($file);
          Image::make($file)->resize(200,200);
          $file->move($parameters['destination'],$parameters['name']);
          return  $parameters;
      }


      $parameters = $this->getDestination($file);

      $file->move($parameters['destination'], $parameters['name']);
    }
    //echo $parameters['imgSrc'];exit;
        return [
            'filelink' => $parameters['imgSrc'],
            'name'     => $parameters['name']
        ];
    }
    protected function getDestination(UploadedFile $file)
    {
          $randomFileName = time() . '.' . str_random(16) . '.' . $file->guessExtension();

          return [
              'destination' => public_path() . $this->uploadPath,
              'name' => $randomFileName,
              'imgSrc' => url($this->uploadPath . $randomFileName),
          ];
      }
}
