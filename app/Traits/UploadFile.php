<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Request;
use Validator;
use Image;
use File;

trait UploadFile{

    public function uploadFile($request,$input_name,$file_upload_path,$remove_file_name = null){
       $file                      = $request->file($input_name);	
       $document_application_path = public_path().'/uploads/'.$file_upload_path;
       File::isDirectory($document_application_path) or File::makeDirectory($document_application_path, 0777, true, true);
       $file_name                 =    mt_rand().'_'.$file->getClientOriginalName();
       $file->move($document_application_path,$file_name);
       // Remove file 
       if(isset($remove_file_name)){
      	 $file_path = public_path().'/uploads/'.$file_upload_path.''.$remove_file_name;
      	  if(file_exists($file_path)){
      	  	 unlink($file_path);
      	  }
       }
      return $file_name;
    }

    public function uploadMultipleImages($file, $path, $remove_file_name = null){
        $permitted_chars            =   '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $allowedfileExtension       =    ['jpg','jpeg','png'];
        $extension                  =    $file->getClientOriginalExtension();
        $check                      =    in_array($extension,$allowedfileExtension);
        if($check){
           $file_size               =    $file->getSize();
           $total_file_size         =    ($file_size)/1024;
           $total_file_size         =    ($total_file_size)/1024;
             if($total_file_size != 0 && $total_file_size < 2){
                $document_application_path  = public_path().'/uploads/'.$path;
                File::isDirectory($document_application_path) or File::makeDirectory($document_application_path, 0777, true, true);
                $file_name                 =    mt_rand().'_'.__generateRandomStringToken($permitted_chars, 20).'.'.$file->getClientOriginalExtension();
                $file->move($document_application_path,$file_name);
                return $file_name; 
              }  
         } 
     return false;    
   }


}	