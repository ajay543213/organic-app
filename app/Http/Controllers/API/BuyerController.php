<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Model\Api\BuyerEnquiery;
use App\Model\Api\TeaBoardOffice;

class BuyerController extends Controller{
    
    public function setBuyerEnquiry(Request $request){
    	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data['Data'], [
	  	  	      'userId'           => ['required', 'regex:/[0-9]/'],
	  	  	      'buyerPhoneNum'    => ['required', 'regex:/[0-9]/'],
	  	  	      'buyerPurpose'     => ['required'],
	              'buyerType'        => ['required', 'regex:/[0-9]/'],
	  	  	      'cropQunatity'     => ['required', 'regex:/^\d+(\.\d{1,2})?$/', 'max:12'],
	              'cropType'         => ['required', 'regex:/[0-9]/'],
	              'lat'              => ['required', 'regex:/^\d+(\.\d)/'],
	              'lng'              => ['required', 'regex:/^\d+(\.\d)/']
          ]);
	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }

	  	  $obj = new BuyerEnquiery();
	  	  $obj->userId           =  $req_data['Data']['userId'];
	  	  $obj->buyerPhoneNum    =  $req_data['Data']['buyerPhoneNum'];
	  	  $obj->buyerPurpose     =  $req_data['Data']['buyerPurpose'];
	  	  $obj->buyerType        =  $req_data['Data']['buyerType'];
	  	  $obj->cropQunatity     =  $req_data['Data']['cropQunatity'];
	  	  $obj->cropType         =  $req_data['Data']['cropType'];
	  	  $obj->lat              =  $req_data['Data']['lat'];
	  	  $obj->lng              =  $req_data['Data']['lng'];
	  	  $obj->timestamp        =  time();
	  	  $res = $obj->save();

	  	  if($res){
	  	  	 return response()->json(['Status' => 10001, 'Data' => $obj->buyerEnquiryId]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10000, 'Message' => 'Try again!.']); 
    }

    public function getTeaBoardOffice(Request $request){
          $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data['Data'], [
	  	  		  'userId' => ['required', 'regex:/[0-9]/'],
	  	  	      'lat'    => ['required', 'regex:/^\d+(\.\d)/'],
	  	  	      'lng'    => ['required', 'regex:/^\d+(\.\d)/']
          ]);
	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }
	  	  $records = TeaBoardOffice::select('teaBoardOfficeId','officeName','officeDetail','lat','lng')->get();
	  	  if($records->count() >0){
	  	  	$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('teaBoardOfficeId'=> (int)$value->teaBoardOfficeId,
	  	  						   'officeName'=>$value->officeName,
	  	  						   'officeDetail'=>$value->officeDetail,
	  	  						   'lat'=>(double) $value->lat,
	  	  						   'lng'=>(double) $value->lng,
	  	  					     );
	  	  	}
	  	  	 return response()->json(['Status' => 10001, 'Data' => $result]);
	  	  }	  
	   return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);		
    }

}
