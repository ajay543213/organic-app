<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Model\Api\Product;

class ProductDetailsController extends Controller{
    
    public function getProducts(Request $request){
     	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data, [
	  	  	      'Data'       => ['required', 'regex:/[0-9]/'],
	  	  	      'pageCount'  => ['required', 'regex:/[0-9]/'],
	  	  	      'pageSize'   => ['required', 'regex:/[0-9]/']
          ]);

	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }

	  	  if(!empty($req_data['pageCount'])){
	  	  	 if($req_data['pageCount'] != 0){
	  	  	    $limit = $req_data['pageCount'];	 	
	  	  	 }else{
	  	  	 	$limit = 0;
	  	  	 }
	  	  }else{
			$limit = 0;		  	  	
	  	  }
	  	  $page    = !empty($req_data['pageSize']) ? $req_data['pageSize'] : 1;
	  	  $offset  = ($page-1) * $limit;

	  	  //dd($offset,$page);
	  	  $records = Product::select(
	  	  	  					    'productId',
	  	  	                        'productName',
	  	  	                        'productPrice',
	  	  	                        'currency',
	  	  	                        'season',
	  	  	                        'place',
	  	  	                        'medicinalValue',
	  	  	                        'imageUrl'
	  	  	                      )
	  	  						->limit($page)
	  	  						->offset($offset)
	  	  						->get();
	  	  if($records->count() > 0){
	  	  	$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('productId'=>$value->productId,
	  	  						   'productName'=>$value->productName,
	  	  						   'productPrice'=> (double)$value->productPrice,
	  	  						   'currency'=>$value->currency,
	  	  						   'season'=>$value->season,
	  	  						   'medicinalValue'=>$value->medicinalValue,
	  	  						   'imageUrl'=>$value->imageUrl,
	  	  							);
	  	  	}
	  	  	 return response()->json(['Status' => 10001, 'Data' => $result]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']); 
     }
}
