<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Model\Api\Location;
use JWTFactory;
use JWTAuth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller{
    	

	  public function saveUserData(Request $request){
	  	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data['Data'], [
	  	  	      'address'      => ['required', 'string', 'max:250'],
	  	  	      'country'      => ['required', 'string', 'max:100'],
	  	  	      'emailId'      => ['required', 'string', 'email', 'max:255'],
	              'fullName'     => ['required', 'string', 'max:255'],
	              'phoneNum'     => ['required', 'regex:/[+][0-9]{3}/', 'max:12'],
	              //'timestamp'    => ['required'],
	              'userType'     => ['required', 'max:100'],
	              //'androidIid'  => [''],
	              //'gcmId'      => ['']
          ]);
	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first(), 'Data' => null]);
	  	  }
	  	  
	  	  $obj = User::where('phoneNum',$req_data['Data']['phoneNum'])->first();
	  	  if(!isset($obj)){
	  	    $check_email_uniq = User::where('emailId', $req_data['Data']['emailId'])->get()->toArray();
	  	     if(count($check_email_uniq)){
	  	   		return response()->json([
	  	   			  'Status' => 10000,
	  	   			  'Message' => 'Email id is already exists!',
	  	   			  'Data' => null
	  	   		]);  	
	  	     }
	  	    $obj = new User();	
	  	    $obj->phoneNum    =  $req_data['Data']['phoneNum'];
	  	  }else{
	  	  	$check_email_uniq = User::where('emailId', $req_data['Data']['emailId'])->where('userId', '!=', $obj->userId)->get()->toArray();
		  	  	if(count($check_email_uniq)){
		  	   		return response()->json([
		  	   			  'Status' => 10000,
		  	   			  'Message' => 'Email id is already exists!',
		  	   			  'Data' => null
		  	   		]);  	
		  	     }
	  	  }  
	  	 
	  	  $obj->fullName    =  $req_data['Data']['fullName'];
	  	  $obj->emailId     =  $req_data['Data']['emailId'];
	  	  $obj->userType    =  $req_data['Data']['userType'];
	  	  $obj->address     =  $req_data['Data']['address'];
	  	  $obj->country     =  $req_data['Data']['country'];
	  	  $obj->createdAt   =  time();
	  	  $obj->updatedAt   =  time();
	  	  $obj->androidId   =  !empty($req_data['Data']['androidId']) ? $req_data['Data']['androidId'] : NULL;
	  	  $obj->fcmId       =  !empty($req_data['Data']['fcmId']) ? $req_data['Data']['fcmId'] : NULL;
	  	  $obj->licenceId   =  !empty($req_data['Data']['licenceKey']) ? $req_data['Data']['licenceKey'] : NULL;
	  	  //$obj->timestamp   =  $req_data['Data']['timestamp']; 
	  	  $token = md5($req_data['Data']['phoneNum']);
	 	  $expiry_time  = date('Y-m-d h:i:s', strtotime('+30 days'));
	 	  $obj->token = $token;
	 	  $obj->token_expiry_time = $expiry_time;

	  	  $res = $obj->save();
          $result = array();
          if($res){
          	  $record = User::select('userId','fullName','emailId','address','country',
		  	                    'phoneNum','androidId',
		  	                    'fcmId','userType','createdAt','updatedAt','token')
          	                  ->where('userId', $obj->userId)
          	                  ->first();

          $result['userId']    = $record->userId;
          $result['fullName']  = $record->fullName;
          $result['emailId']   = $record->emailId;
          $result['country']   = $record->country;
          $result['phoneNum']  = $record->phoneNum;
          $result['androidId'] = $record->androidId;
          $result['fcmId']     = $record->fcmId??'';
          $result['userType']  = (int) $record->userType;
          $result['createdAt'] = (int) $record->createdAt;
          $result['updatedAt'] = (int) $record->updatedAt;
       
         return response()->json(['Status' => 10001, 'Data' => $result, 'token' => $record->token]);
       }

         return response()->json(['Status' => 10000, 'Message' => 'Try again!.', 'Data' => null]);
	 }

	 public function getUserData(Request $request){
	 	 $record   = array();
	 	 $req_data = $request->json()->all();
	 	 $mob_num  = $req_data['Data'];
	 	 
	 	 // if(strlen($mob_num) == 13){
	 	 // 	$mob_num = substr($mob_num, 3);
	 	 // }else if($mob_num < 10 && $mob_num > 13){
	 	 //  return response()->json(['Status' => 10000, 'Message' => 'Please enter valid data.', 'Data' => $record]);
	 	 // }

		  $record = User::select('userId','fullName','emailId','address','country',
		  	                   'phoneNum','androidId',
		  	                    'userType','createdAt','updatedAt')
		 	                  ->where('phoneNUM', $mob_num)
		 	                  ->first();
		 $result = [];	                  
	 	 if(isset($record)){
	 	 	 $token = md5($mob_num);
	 	 	 $expiry_time  = date('Y-m-d h:i:s', strtotime('+30 days'));
	 	 	 $record->token = $token;
	 	 	 $record->token_expiry_time = $expiry_time;
	 	 	 $record->save();
	 	 	 
	 	 	 $record = User::select('userId','fullName','emailId','address','country',
		  	                   'phoneNum','androidId',
		  	                    'userType','createdAt','updatedAt')
		 	                  ->where('phoneNUM', $mob_num)
		 	                  ->first();

		 	    $result['userId']    = $record->userId;
          	    $result['fullName']  = $record->fullName;
          	    $result['emailId']   = $record->emailId;
          	    $result['country']   = $record->country;
          	    $result['phoneNum']  = $record->phoneNum;
          	    $result['androidId'] = $record->androidId;
          	    $result['fcmId']     = $record->fcmId??'';
          	    $result['userType']  = (int) $record->userType;
          	    $result['createdAt'] = (int) $record->createdAt;
          	    $result['updatedAt'] = (int) $record->updatedAt;             
		     return response()->json(['Status' => 10001, 'Data' => $result, 'Token' => $token]);
          }	 

	 	return response()->json(['Status' => 10006, 'Message' => 'No record found.', 'Data' => null]);    
	 } 

     public function setLocation(Request $request){
     	 $req_data = $request->json()->all();
	  	 $validator =  Validator::make($req_data['Data'], [
	  	  	      'latitude'     => ['required', 'max:20'],
	  	  	      'longitude'    => ['required', 'max:20'],
	              'userId'       => ['required', 'regex:/[0-9]/'],
         ]);

	  	 if($validator->fails()){
	  	   return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	 }

	  	 $obj = new Location();
	  	 $obj->lat     = $req_data['Data']['latitude'];
	  	 $obj->lng     = $req_data['Data']['longitude'];
	  	 $obj->user_id = $req_data['Data']['userId'];
	  	 $res = $obj->save();

	  	 if($res)
	  	 return response()->json(['Status' => 10001, 'Data' => true]);
        else
   		 return response()->json(['Status' => 10006, 'Data' => false]);
     }      
       
}
