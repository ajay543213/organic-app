<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Model\Api\FarmerProductSellRequest;
use App\Model\Api\FarmerRequest;
use App\Model\Api\Receipe;
use App\Model\Api\OraganicStore;

use App\Model\Api\OrganicFarmingVideo;
use App\Model\Api\OrganicUttrakhandSupport;
use App\Model\Api\CAOData;
use App\Model\Api\FarmerFederations;

class FarmerProductRequestDetails extends Controller{
    
	 public function setFarmerRequest(Request $request){
          $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data['Data'], [
	  	  	      'userId'            => ['required', 'regex:/[0-9]/'],
	  	  	      'adminId'           => ['required', 'regex:/[0-9]/'],
	  	  	      'farmerLat'         => ['required', 'regex:/^\d+(\.\d)/'],
	              'farmerLng'         => ['required', 'regex:/^\d+(\.\d)/'],
	  	  	      'farmerPhoneNum'    => ['required', 'min:10', 'regex:/[0-9]{10}/', 'max:12'],
	  	  	      'requestStartDate'  => ['required', 'regex:/[0-9]/'],
	              'requestStatus'     => ['required', 'regex:/[0-9]/'],
	              'requestType'       => ['required', 'regex:/[0-9]/']
          ]);

	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }

	  	  $obj = new FarmerRequest();
	  	  $obj->userId           =  $req_data['Data']['userId'];
	  	  $obj->adminId          =  $req_data['Data']['adminId'];
	  	  $obj->farmerLat        =  $req_data['Data']['farmerLat'];
	  	  $obj->farmerLng        =  $req_data['Data']['farmerLng'];
	  	  $obj->farmerPhoneNum   =  $req_data['Data']['farmerPhoneNum'];
	  	  $obj->requestStartDate =  $req_data['Data']['requestStartDate'];
	  	  $obj->requestStatus    =  $req_data['Data']['requestStatus'];
	  	  $obj->requestType      =  $req_data['Data']['requestType'];
	  	  $res = $obj->save();

	  	  if($res){
	  	  	 return response()->json(['Status' => 10001, 'Data' => $obj->requestId]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10000, 'Message' => 'Try again!.']);
	 }


     public function setFarmerProductSellRequest(Request $request){
     	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data['Data'], [
	  	  	      'userId'          => ['required', 'regex:/[0-9]/'],
	  	  	      'farmerPhoneNum'  => ['required', 'min:10', 'regex:/[0-9]{10}/', 'max:12'],
	  	  	      'quantityInKg'    => ['required', 'regex:/^\d+(\.\d{1,2})?$/'],
	              'pricePerKg'      => ['required', 'regex:/^\d+(\.\d{1,2})?$/'],
	              'lat'             => ['required', 'regex:/^\d+(\.\d)/'],
	              'lag'             => ['required', 'regex:/^\d+(\.\d)/'],
	              'fieldOfficerId'  => ['required', 'regex:/[0-9]/'],
	              'poductId'        => ['required', 'regex:/[0-9]/']
          ]);

	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }

	  	  $obj = new FarmerProductSellRequest();
	  	  $obj->userId           =  $req_data['Data']['userId'];
	  	  $obj->farmerPhoneNum   =  $req_data['Data']['farmerPhoneNum'];
	  	  $obj->quantityInKg     =  $req_data['Data']['quantityInKg'];
	  	  $obj->pricePerKg       =  $req_data['Data']['pricePerKg'];
	  	  $obj->lat              =  $req_data['Data']['lat'];
	  	  $obj->lng              =  $req_data['Data']['lag'];
	  	  $obj->fieldOfficerId   =  $req_data['Data']['fieldOfficerId'];
	  	  $obj->poductId         =  $req_data['Data']['poductId'];
	  	  $res = $obj->save();

	  	  if($res){
	  	  	 return response()->json(['Status' => 10001, 'Data' => $obj->prodcutSellReqId]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10000, 'Message' => 'Try again!.']); 
     }


     public function getRecipe(Request $request){
    	$req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data, [
	  	  	      'Data'       => ['required', 'regex:/[0-9]/'],
	  	  	      'pageCount'  => ['required', 'regex:/[0-9]/'],
	  	  	      'pageSize'   => ['required', 'regex:/[0-9]/']
          ]);

	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }

	  	  $records = Receipe::select('recipeId','recipeName','recipeDetail')->get()->toArray();
	  	  if(count($records)){
	  	  	 return response()->json(['Status' => 10001, 'Data' => $records]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);
     }  

     public function getOrganicStores(Request $request){
     	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data['Data'], [
	  	  		  'userId' => ['required', 'regex:/[0-9]/'],
	  	  	      'lat'    => ['required', 'regex:/^\d+(\.\d)/'],
	  	  	      'lng'    => ['required', 'regex:/^\d+(\.\d)/']
          ]);

	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }

	  	  $records = OraganicStore::select('organicStoreId','organicStoreName','organicStoreDetail','lat','lng')->get();
	  	  if($records->count() >0){
	  	  	$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('organicStoreId'=>$value->organicStoreId,
	  	  						   'organicStoreName'=>$value->organicStoreName,
	  	  						   'organicStoreDetail'=>$value->organicStoreDetail,
	  	  						   'lat'=>(double) $value->lat,
	  	  						   'lng'=>(double) $value->lng,
	  	  							);
	  	  	}
	  	  	 return response()->json(['Status' => 10001, 'Data' => $result]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);
     }

     public function getFarmerFederations(Request $request){
     	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data, [
	  	  		  'Data'       => ['required', 'regex:/[0-9]/'],
	  	  	      'pageCount'  => ['required', 'regex:/[0-9]/'],
	  	  	      'pageSize'   => ['required', 'regex:/[0-9]/']
          ]);
	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }
	  	  $records = FarmerFederations::select('farmerFederationId',
	  	  	                                      'farmerFederationName',
	  	  	                                      'farmerFederationDistrict',
	  	  	                                      'farmerFederationBlock',
	  	  	                                      'farmerFederationTrainerName'
	  	  	                                  )->get();
	  	  if($records->count() >0){
	  	  	$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('farmerFederationId'          => (int)$value->farmerFederationId,
	  	  						  'farmerFederationName'        => $value->farmerFederationName,
	  	  						  'farmerFederationDistrict'    => $value->farmerFederationDistrict,
	  	  						  'farmerFederationBlock'       => $value->farmerFederationBlock,
	  	  						  'farmerFederationTrainerName' => $value->farmerFederationTrainerName 
	  	  						 );
	  	  	}
	  	  	 return response()->json(['Status' => 10001, 'Data' => $result]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);
     }

     public function getOrganicFarmingVideo(Request $request){
     	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data, [
	  	  		  'Data'       => ['required', 'regex:/[0-9]/'],
	  	  	      'pageCount'  => ['required', 'regex:/[0-9]/'],
	  	  	      'pageSize'   => ['required', 'regex:/[0-9]/']
          ]);
	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }
	  	  $records = OrganicFarmingVideo::select('videoId','videoUrl')->get();
	  	  if($records->count() >0){
	  	  	$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('videoId'  => (int)$value->videoId,
	  	  						  'videoUrl' => $value->videoUrl
	  	  						 );
	  	  	}
	  	  	 return response()->json(['Status' => 10001, 'Data' => $result]);
	  	  }
	  	  
	  	 return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);
     }

     public function getOrganicUttrakhandSupport(Request $request){
          $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data, [
	  	  		  'Data'       => ['required', 'regex:/[0-9]/']
          ]);
	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }
	  	  $records = OrganicUttrakhandSupport::select('supportPhoneNumber','enquiryPhoneNumber')->get();
	  	  if($records->count() >0){
	  	  	$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('supportPhoneNumber'  => $value->supportPhoneNumber,
	  	  						  'enquiryPhoneNumber'  => $value->enquiryPhoneNumber
	  	  						 );
	  	  	}
	  	  	 return response()->json(['Status' => 10001, 'Data' => $result]);
	  	  }
	  	  
	  	return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);
     }

     public function getCAOData(Request $request){
     	  $req_data = $request->json()->all();
	  	  $validator =  Validator::make($req_data['Data'], [
	  	  		  'userId'  => ['required', 'regex:/[0-9]/'],
	  	  	      'lat'     => ['required', 'required', 'regex:/^\d+(\.\d)/'],
	  	  	      'lng'     => ['required', 'required', 'regex:/^\d+(\.\d)/']
          ]);

	  	  if($validator->fails()){
	  	  	  return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	  }

	  	  $records = CAOData::select('caoId','name', 'designation', 'locationName', 'lat', 'lng')->get();
	  	  if($records->count() >0){
	  	  	$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('caoId'         =>  (int)$value->caoId,
	  	  						  'name'          =>  $value->name,
	  	  						  'designation'   =>  $value->designation,
	  	  						  'locationName'  =>  $value->locationName,
	  	  						  'lat'           =>  (double)$value->lat, 
	  	  						  'lng'           =>  (double)$value->lng
	  	  						 );
	  	  	}
	  	  	 return response()->json(['Status' => 10001, 'Data' => $result]);
	  	  }
	  	  
	  	return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);	
     }

     public function getFarmerData(Request $request){
     	
     }

     public function setFcmId(Request $request){
     	
     }

     public function facilityAndFee(Request $request){

     }
}
