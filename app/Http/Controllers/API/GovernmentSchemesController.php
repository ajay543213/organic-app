<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Model\Api\GovernmentSchemes;

class GovernmentSchemesController extends Controller{
    
    public function getGovernmentScheme(Request $request){
    	$req_data = $request->json()->all();
	  	$validator =  Validator::make($req_data, [
	  	  	   'Data'       => ['required', 'regex:/[0-9]/'],
	  	  	   'pageCount'  => ['required', 'regex:/[0-9]/'],
	  	  	   'pageSize'   => ['required', 'regex:/[0-9]/']
        ]);

	  	if($validator->fails()){
	  	   return response()->json(['Status' => 10000, 'Message' => $validator->errors()->first()]);
	  	}

	  	$records = GovernmentSchemes::select('schemeId','schemeName','schemeStartDate','schemeEndDate','lat','lng')->get();
	  	if($records->count() >0){
	  		$result = [];
	  	  	foreach ($records as $key => $value) {
	  	  		$result[] = array('schemeId'=>$value->schemeId,
	  	  						   'schemeName'=>$value->schemeName,
	  	  						   'schemeStartDate'=>strtotime($value->schemeStartDate),
	  	  						   'schemeEndDate'=>strtotime($value->schemeEndDate),
	  	  						   'lat'=>(double) $value->lat,
	  	  						   'lng'=>(double) $value->lng,
	  	  							);
	  	  	}
	  	  return response()->json(['Status' => 10001, 'Data' => $result]);
	  	}
	  	  
	  	return response()->json(['Status' => 10006, 'Message' => 'No Record Found.']);
    }
}
