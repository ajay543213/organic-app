<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Model\User\RoleDetails;
use App\Model\User\PermissionDetails;

class RolesDetailsController extends Controller{
    
    public $redirect_url;


      public function __construct(){
         $this->middleware('auth');
         $this->redirect_url = '/users-management/roles/list';
      }

      public function index(){
      	 $records = RoleDetails::where('name', '!=', 'admin')->get();
      	 return view('admin.users-management.roles.role_list',compact('records'));
      }

      public function add(){
      	 return view('admin.users-management.roles.role_add');
      }

      public function save(Request $request){
         $validator = $this->validateForm($request);
         if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());	 
         }
         if($this->checkUniqueRole($request->name)){
            return redirect()->back()->withInput($request->all())->with('error','Role is already define.');
         }
         $res = $this->store(new RoleDetails, $request);
         if($res){
         	return redirect($this->redirect_url)->with('success','Record has been successfully created.');
         }
        return redirect()->back()->withInput($request->all())->with('error','Something went wrong! Try again.'); 
      }

      public function edit($id){
      	 $record = RoleDetails::find($id);
      	 return view('admin.users-management.roles.role_add',compact('record')); 
      }

      public function update(Request $request){
         $validator = $this->validateForm($request);
         if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());	 
         }
         $id = $request->hidden_id;
         if($this->checkUniqueRole($request->name, $id)){
            return redirect()->back()->withInput($request->all())->with('error','Role is already define.');
         }
         $res = $this->store(RoleDetails::find($id), $request);
         if($res){
         	return redirect($this->redirect_url)->with('success','Record has been successfully updated.');
         }
        return redirect()->back()->withInput($request->all())->with('error','Something went wrong! Try again.');	 
      }

      public function delete($id){
      	 if($id != 1){
      	   RoleDetails::where('id',$id)->delete();
      	   return redirect($this->redirect_url)->with('success','Record has been successfully deleted.');
      	 }  
      	return redirect()->back()->with('error','Something went wrong! Try again.'); 
      }

      protected function validateForm($request){
         return Validator::make($request->all(),[
         	    'name' => ['required', 'string', 'max:30']
         ]);
      }

      protected function store($obj, $request){
         $obj->name = $request->name;
         return $obj->save();
      }

      protected function checkUniqueRole($name, $id = null){
      	  if($id){
      	      $record = RoleDetails::where('name',$name)->where('id', '!=', $id)->get()->toArray();
      	   }else{
              $record = RoleDetails::where('name',$name)->get()->toArray();
      	  }
        return count($record);
      }
}
