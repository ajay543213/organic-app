<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use File;
use App\User;
use App\Model\User\RoleDetails;

class UserDetailsController extends Controller{
    
      public $redirect_url;


      public function __construct(){
         $this->middleware('auth');
         $this->redirect_url = '/users-management/users/list';
      }

      public function index(){
         $records = User::where('id','!=',1)->get();
         return view('admin.users-management.users.list_users',compact('records'));
      }

      public function registerForm(){
         $roles = RoleDetails::where('id','!=',1)->get();
      	 return view('admin.users-management.users.register_user',compact('roles'));
      }      

      public function registerNewUser(Request $request){
         $validator = $this->validatorSave($request);
         if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
         }

         $obj = new User();
         $res = $this->store($request->all(),$obj);
         if($res){
         	return redirect($this->redirect_url)->with('success','Record has been successfully saved.');
         }

         return redirect()->back()->with('error','Try again!');
      }

      public function editUserDetails($id){
         $record = User::findOrFail($id);
         $roles  = RoleDetails::where('id','!=',1)->get();
         return view('admin.users-management.users.register_user',compact('record','roles'));
      }

      public function updateUserDetails(Request $request){
         $validator = $this->validatorUpdate($request);
         if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
         }
         $id = $request->user_id;
         if(!isset($id)){
         	return redirect()->back()->with('error','Try again!.');
         }
         $records = User::where('email',$request->email)->where('id','!=',$id)->get()->toArray();
         if(count($records)){
            return redirect()->back()->with('error','Email Id already exists!.');	 
         }
         $obj           = User::findOrFail($id);
         $obj->name     = $request->name;
         $obj->email    = $request->email;
         $obj->role_id  = $request->role;
         $obj->save();
         return redirect($this->redirect_url)->with('success','Record has been successfully updated.');
      }

      public function deleteUserDetails($id){
      	 User::where('id',$id)->delete();
         return redirect($this->redirect_url)->with('success','Record has been successfully deleted.');
      }

      public function validatorSave($request){
        return Validator::make($request->all(), [
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role'     => ['required']
        ]);
      }

      public function validatorUpdate($request){
        return Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'role'     => ['required']
        ]);
      }

      public function store($array, $obj){
         $obj->name      = $array['name'];
         $obj->email     = $array['email'];
         $obj->password  = Hash::make($array['password']);
         $obj->role_id   = $array['role'];
         return $obj->save();
      }

      public function editProfile(){
         if(Auth::user()){
            $record = User::findOrFail(Auth::user()->id);
            return view('admin.users-management.users.manage_user_profile',compact('record'));
         }
      }

      public function updateProfile(Request $request){
          $validator = Validator::make($request->all(),[
                'name'              => ['required', 'string', 'max:30'],
                'current_password'  => ['required', 'string', 'min:8', 'max:15'],
                'password'          => ['required', 'string', 'min:8', 'confirmed'],
          ]);

          if($validator->fails()){
             return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
          }   
          
          if(!Hash::check($request->current_password, auth()->user()->password)){
             return redirect()->back()->withInput($request->all())->with('error','Current password is incorrect!.');
          }

          $record               = User::find(Auth::user()->id);  

           if ($request->hasFile('profile_pic')){
             $return_value         = $this->upload_image($request);
               if($return_value == 'size') {
                  return redirect()->back()->withInput($request->all())->with('error','Max file size allowed 2MB.');
               }elseif($return_value == 'type'){
                 return redirect()->back()->withInput($request->all())->with('error','Select only [jpg,png,jpeg] file.');
               }
             $file_name  = $return_value;
             if(isset($record->profile_pic)){
                $res = $this->deleteProfilePic($record->profile_pic);    
             }
           }else{
              $file_name = isset($record->profile_pic) ? $record->profile_pic : null;
          }  

          $record->name         = $request->name;
          $record->password     = Hash::make($request->password);
          $record->profile_pic  = $file_name;
          $record->save();

          return redirect()->back()->with('success','Profile details has been successfully updated.');
      }   

      public function upload_image($request){
            if ($request->hasFile('profile_pic')){
               $allowedfileExtension           =    ['jpg','png','jpeg','gif'];
               $file                           =    $request->file('profile_pic');
               $extension                      =    $file->getClientOriginalExtension();
               $check                          =    in_array($extension,$allowedfileExtension);
                  if(!$check){
                    return "type";
                  }
                  else{
                     $document_application_path = public_path().'/uploads/users/profile/';
                     File::isDirectory($document_application_path) or File::makeDirectory($document_application_path, 0777, true, true);
                     $file_name                      =    mt_rand().'_'.$file->getClientOriginalName();
                     $file->move($document_application_path,$file_name);
                     return $file_name; 
                  }

        }
      return 'NULL';
    }

    public function deleteProfilePic($file_name){
        $document_application_path = public_path().'/uploads/users/profile/'.$file_name;
        if(file_exists($document_application_path)){
           unlink($document_application_path);
        }
       return null; 
    }
} 
