<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request as HttpRequest;
use Validator;
use Session;
use Mail;
use Auth;
use App\User;
use App\Model\Configuration\ContactDetails;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function resetPassword(){
       if(Auth::check()){
          return redirect('/home');
       } 
       return view('auth.passwords.email');
    }

    public function resetPasswordSentMailToUser(HttpRequest $request){
          $validator = Validator::make($request->all(),[
                          'email' => ['required', 'string', 'email', 'max:30']     
                       ]);
          if($validator->fails()){
             return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
          }

          $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $record   = User::where('email',$request->email)->first();

          if(!isset($record)){
             return redirect()->back()->withInput($request->all())->with('error',"Email Id is incorrect! Please enter valid email id.");
          } 

         $array_data['user_details']    = $record->toArray(); 
         $array_data['company_details'] = ContactDetails::orderBy('id','desc')->first()->toArray();
         $array_data['rand_char']       = __generateRandomStringToken($permitted_chars, 20);
         
         // return view('auth.passwords.reset_password_email_template',['array_data'=>$array_data]);
         // die;
         Mail::send('auth.passwords.reset_password_email_template',['array_data'=>$array_data],function($message) use ($request) {
             $message->to($request->email, 'Please Dial');
             $message->subject('Reset Password Mail');
         });

         $record->forgot_password_token = $array_data['rand_char'];
         $record->save();

         return redirect()->back()->with('success','Reset password link has been sent on your email.');
    }

    public function resetPasswordCheckToken($token){
       $record = User::where('forgot_password_token',$token)->first();
       if(!isset($record)){
          return redirect('reset/password')->with('error','Sorry Try again!');
       }
       return view('auth.passwords.reset_password_change',compact('record'));
    }

    public function resetPasswordUpdate(HttpRequest $request){
      if(!isset($request->id)){
         return redirect()->back()->with('error','Try again!');
      }
      if($request->password != $request->confirm_password){
        return redirect()->back()->with('error','Password and Confirm Password not matched!.');
      }
      $record = User::find($request->id);
      if(!isset($record->forgot_password_token)){
        return redirect()->back()->with('error','Try again!.');
      }
      $record->password               = Hash::make($request->password);
      $record->forgot_password_token  = NULL;
      $record->save();
      return redirect('/login')->with('success','Password has been successfully updated.');
    }

}
