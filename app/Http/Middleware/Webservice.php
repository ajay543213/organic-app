<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\User;
class Webservice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!empty($request->header('token'))){
           $record = User::where('token', $request->header('token'))->first();
           if(isset($record)){
              if(strtotime($record->token_expiry_time) < strtotime(date('Y-m-d h:i:s'))){
                 return response()->json([
                  'status'    => 10009,
                  'message'   => 'Token Expired.',
                  'error'     => 'invalid_request',
                ], 401);   
              }  
           }else{
              return response()->json([
                'status'    => 10003,
                'message'   => 'Unauthorised access.',
                'error'     => 'invalid_request',
             ], 401);  
           }
        }else{
            return response()->json([
                'status'    => 10003,
                'message'   => 'Unauthorised access.',
                'error'     => 'invalid_request',
            ], 401);
        }

        return $next($request);
    }
}