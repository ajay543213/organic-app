<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class PermissionDetails extends Model{
    
    protected $table     = 'permissions';
    protected $fillable  = ['*'];
}
