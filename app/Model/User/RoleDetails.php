<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class RoleDetails extends Model{
    
    protected $table     = 'roles';
    protected $fillable  = ['*'];
}
