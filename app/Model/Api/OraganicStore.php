<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class OraganicStore extends Model{
    protected $table      = 'organic_stores';
    protected $fillable   = ['*'];
    protected $primaryKey = 'organicStoreId';
    public $timestamps    = false;
}
