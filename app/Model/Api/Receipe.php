<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class Receipe extends Model{
    protected $table      = 'recipes';
    protected $fillable   = ['*'];
    protected $primaryKey = 'recipeId';
    public $timestamps    = false;
}
