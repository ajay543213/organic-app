<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class TeaBoardOffice extends Model{
    protected $table      = 'tea_board_office';
    protected $fillable   = ['*'];
    protected $primaryKey = 'teaBoardOfficeId';
    public $timestamps    = false;
}
