<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    protected $table      = 'products';
    protected $fillable   = ['*'];
    protected $primaryKey = 'productId';
    public $timestamps    = false;
}
