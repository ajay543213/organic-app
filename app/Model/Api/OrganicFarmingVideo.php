<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class OrganicFarmingVideo extends Model{
    protected $table      = 'organic_farming_video';
    protected $fillable   = ['*'];
    public $timestamps    = false;
}
