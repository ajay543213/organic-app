<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class FarmerProductSellRequest extends Model{
	protected $table      = 'famer_product_sell_request';
    protected $fillable   = ['*'];
    protected $primaryKey = 'prodcutSellReqId';
    public $timestamps    = false;
}
