<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class GovernmentSchemes extends Model{
    protected $table      = 'government_schemes';
    protected $fillable   = ['*'];
    protected $primaryKey = 'schemeId';
    public $timestamps    = false;
}
