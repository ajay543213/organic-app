<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class FarmerRequest extends Model{    
    protected $table      = 'farmer_requests';
    protected $fillable   = ['*'];
    protected $primaryKey = 'requestId';
    public $timestamps    = false;
}
