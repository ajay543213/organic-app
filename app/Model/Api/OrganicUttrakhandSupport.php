<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class OrganicUttrakhandSupport extends Model{
    protected $table      = 'organic_uttrakhand_supports';
    protected $fillable   = ['*'];
    public $timestamps    = false;
}
