<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table      = 'locations';
    protected $fillable   = ['*'];
    protected $primaryKey = 'locationId';
    public $timestamps = false;
}
