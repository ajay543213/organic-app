<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class FarmerFederations extends Model{
    protected $table      = 'farmer_federations';
    protected $fillable   = ['*'];
    protected $primaryKey = 'farmerFederationId';
    public $timestamps    = false;
}
