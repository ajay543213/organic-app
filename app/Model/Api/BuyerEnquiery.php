<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class BuyerEnquiery extends Model{
    protected $table      = 'buyer_enquiry';
    protected $fillable   = ['*'];
    protected $primaryKey = 'buyerEnquiryId';
    public $timestamps    = false;
}
