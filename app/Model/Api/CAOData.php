<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class CAOData extends Model{
    protected $table      = 'chief_agriculture_officer';
    protected $fillable   = ['*'];
    protected $primaryKey = 'caoId';
    public $timestamps    = false;
}
